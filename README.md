#Dagger Dungeon

Dagger Dungeon is a simple roguelike game with randomly generated levels and Unicode graphics. The project uses the Lanterna Java library so that it can run in almost any terminal window or create a Swing terminal window if it's not launched from a terminal.

This is just a small personal project but anyone is welcome to contribute if they want to.

#TODO

This is a small list of features I hope to improve/implement:

- Add help window
- Add more enemy types
- Add more weapon types
- Improve combat
- Improve random level generation
- Add tresure chests, these would spawn randomly in some levels and drop weapons or health
- Add bias to level and character generation to make difficult scale up as more levels are completed.
- Add sound

#Screenshots

Dagger Dungeon running in a Swing Terminal Frame:

![](https://bitbucket.org/LackToast/dagger-dungeon/raw/e237a5a00feb768bf9eec5e7c47e3009eadf6bec/Images/DaggerDungeonInSwing.png)

Dagger Dungeon running in a Terminal on Manjaro:

![](https://bitbucket.org/LackToast/dagger-dungeon/raw/e237a5a00feb768bf9eec5e7c47e3009eadf6bec/Images/DaggerDungeonInTerminal.png)

Inventory screen open:

![](https://bitbucket.org/LackToast/dagger-dungeon/raw/e237a5a00feb768bf9eec5e7c47e3009eadf6bec/Images/DaggerDungeonInventory.png)

Exit Message:

![](https://bitbucket.org/LackToast/dagger-dungeon/raw/e237a5a00feb768bf9eec5e7c47e3009eadf6bec/Images/DaggerDungeonExitMessage.png)
