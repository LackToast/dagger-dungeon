package com.lacktoast.daggerdungeon.Characters;

import java.util.concurrent.ThreadLocalRandom;
import com.lacktoast.daggerdungeon.Weapons.Weapon;

public abstract class Characters {
	
	protected int hp;
	protected Weapon equipped;
	protected int rowposition;
	protected int columnposition;
	protected char icon;
	
	
	public Characters(int hp, Weapon weapon) {
		this.hp = hp;
		this.equipped = weapon;
	}
	
	public Characters(int hp) {
		this.hp = hp;
	}
	
	public int getHealth() {
		return hp;
	}
	
	public void takeHealthPotion(int potion) {
		hp += potion;
		if(hp>100)
			hp = 100;
	}
	
	//When a character dies they will drop a health potion that give the character between 0 and 5 health back.
	public int dropHealthPotion() {
		return ThreadLocalRandom.current().nextInt(0,6);
	}
	
	public void takeDamage(int damage) {
		hp -= damage;
	}

	public char getIcon() {
		return icon;
	}
	
	public Weapon getEquipped() {
		return equipped;
	}	
	
	public int getRowPosition() {
		return rowposition;
	}
	
	public int getColumnPosition() {
		return columnposition;
	}
	
	public void setRowPosition(int rowposition) {
		this.rowposition = rowposition;
	}
	
	public void setColumnPosition(int columnposition) {
		this.columnposition = columnposition;
	}
	
	public void moveRight() {
		columnposition++;
	}
	
	public void moveLeft() {
		columnposition--;
	}
	
	public void moveUp() {
		rowposition--;
	}
	
	public void moveDown() {
		rowposition++;
	}
}
