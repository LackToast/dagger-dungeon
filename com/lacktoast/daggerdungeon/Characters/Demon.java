package com.lacktoast.daggerdungeon.Characters;

import java.util.concurrent.ThreadLocalRandom;
import com.lacktoast.daggerdungeon.Weapons.Rapier;
import com.lacktoast.daggerdungeon.Weapons.ShortSword;

//The Demon is the mid-tier enemy. It has 5-10 hp and normally spawns with a Rapier. It has a small chance of spawning with a ShortSword
public class Demon extends Characters{

		public Demon() {
			super(ThreadLocalRandom.current().nextInt(5, 11));
			this.icon = 'd';
			if(ThreadLocalRandom.current().nextInt(1, 11)>8) {
				this.equipped = new ShortSword();
			} else
				this.equipped = new Rapier();
		}
}
