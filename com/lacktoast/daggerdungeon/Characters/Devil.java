package com.lacktoast.daggerdungeon.Characters;

import java.util.concurrent.ThreadLocalRandom;
import com.lacktoast.daggerdungeon.Weapons.LongSword;
import com.lacktoast.daggerdungeon.Weapons.ShortSword;

//The Devil is the strongest enemy. It has 10-20 hp and normally spawns with a ShortSword. It has a small chance of spawning with a LongSword
public class Devil extends Characters{

	public Devil() {
		super(ThreadLocalRandom.current().nextInt(10, 21));
		this.icon = 'D';
		if(ThreadLocalRandom.current().nextInt(1, 11)>8) {
			this.equipped = new LongSword();
		} else
			this.equipped = new ShortSword();
	}
}
