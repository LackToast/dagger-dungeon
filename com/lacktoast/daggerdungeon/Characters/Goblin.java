package com.lacktoast.daggerdungeon.Characters;

import java.util.concurrent.ThreadLocalRandom;

import com.lacktoast.daggerdungeon.Weapons.Dagger;
import com.lacktoast.daggerdungeon.Weapons.Rapier;

//The Goblin is the weakest enemy. It has 1-5 hp and normally spawns with a Dagger. It has a small chance of spawning with a Rapier
public class Goblin extends Characters{

	public Goblin() {
		super(ThreadLocalRandom.current().nextInt(1, 6));
		this.icon = 'G';
		if(ThreadLocalRandom.current().nextInt(1, 11)>8) {
			this.equipped = new Rapier();
		} else
			this.equipped = new Dagger();
	}
}
