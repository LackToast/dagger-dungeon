package com.lacktoast.daggerdungeon.Characters;

import java.util.HashSet;

import com.lacktoast.daggerdungeon.Level;
import com.lacktoast.daggerdungeon.Weapons.*;

public class Player extends Characters{
	
	private HashSet<Weapon> inventory;
	private int level;

	public Player() {
		super(100, new Dagger());
		level = 1;
		rowposition = Level.PLAYERSTARTROW;
		columnposition = Level.PLAYERSTARTCOLUMN;
		icon = Level.PLAYERICON;
		inventory = new HashSet<Weapon>();
		this.weaponPickup(equipped); //add the new dagger to the players inventory
	}
	
	public void weaponPickup(Weapon item) {
			inventory.add(item);
	}
	
	public Object[] getInventoryList() {
		return inventory.toArray();
	}
	
	public int getLevel() {
		return level;
	}
	
	//This method will level up the character and return them to the starting point of the level
	//The player only levels up when they reach the exit to a level
	public void levelUp() {
		level += 1;
		rowposition = Level.PLAYERSTARTROW;
		columnposition = Level.PLAYERSTARTCOLUMN;
	}

	public void equipItem(String item) {
		Object[] list = this.getInventoryList();
		
		for(Object o : list) {
			if(o.toString().equals(item)) {
				Weapon toequip = (Weapon) o;
				this.equipped = toequip;
				break;
			}
		}
	}

}
