package com.lacktoast.daggerdungeon;

import java.io.IOException;
import com.googlecode.lanterna.input.KeyStroke;
import com.lacktoast.daggerdungeon.Characters.Player;

public class Game {
	
	private Player player;
	private Gui gui;
	private Level level;
	
	public Game() {
		player = new Player();
		gui = new Gui();
		level = new Level();
	}
	
	private void handleUserInput(KeyStroke key) throws IOException {
		if(key==null || key.getCharacter() == null)
			return;
		
		switch(key.getCharacter().charValue()) {
			case 'w':
				level.validMoveUp(player);
				break;
			case 's':
				level.validMoveDown(player);
				break;
			case 'd':
				level.validMoveRight(player);
				break;
			case 'a':
				level.validMoveLeft(player);
				break;
			case 'i':
				gui.updateInventory(player);
				gui.accessInventory(player);
				break;
			case ' ':
				level.playerAttack(player);
				break;
			case 'q':
				gui.promptToExit();
		}
		if(checkLevelComplete()) {
			System.out.println("Level "+player.getLevel()+" complete.");
			player.levelUp();
			level = new Level();
			gui.updateMap(level);
		}
	}

	private boolean checkLevelComplete() {
		return player.getColumnPosition() == Level.EXITCOLUMN && player.getRowPosition() == Level.EXITROW;
	}

	//This is the main game loop. It will handle user input, run enemy actions, then update all aspects of the gui.
	public void gameLoop() {
		try {
			//Update the Inventory, stats and map so they are accurate when the gui is first displayed
			gui.updateInventory(player);
			gui.updateStats(player);
			gui.updateMap(level);
			long starttime = System.currentTimeMillis();
			while(true) {
				handleUserInput(gui.getUserInput());
				if(System.currentTimeMillis()-starttime>70) {//We use this to limit the enemies APM
					level.enemyActions(player);
					starttime = System.currentTimeMillis();
				}
				gui.updateMap(level);
				gui.updateStats(player);
				if(checkPlayerHealth()) //This returns true if the player has died and wants to play another game
					break;
				Thread.sleep(30);//This is used to limit player APM
				gui.update();
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	//This is called before the gui is updated on every run of the game loop
	//It checks to make sure the player is still alive and if they aren't
	//it will call the gui to prompt the user to restart. If that call returns
	//the game is restarted
	private boolean checkPlayerHealth() throws IOException {
		if(player.getHealth()<1) {
			System.out.println("You have died.");
			gui.promptToRestart();
			return true;
		}
		return false;
	}

}
