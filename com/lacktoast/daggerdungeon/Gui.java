package com.lacktoast.daggerdungeon;

import java.io.IOException;
import java.util.LinkedList;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.ComboBox;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.TextBox;
import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogBuilder;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogButton;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.gui2.Window.Hint;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.lacktoast.daggerdungeon.Characters.Player;

public class Gui {
	
	private Screen screen;
	private WindowBasedTextGUI gui;
	private ComboBox<String> inventorylist;
	private TextBox stats;
	private TextBox map;
	
	public Gui() {
		inventorylist = new ComboBox<String>();
		map = new TextBox();
		stats = new TextBox();
		screen = null;
		gui = null;
		this.startGUI();
	}
	
	//This method will update the inventory that is displayed in the comboBox to make sure if reflects the players inventory
	public void updateInventory(Player player) {
		Object[] list = player.getInventoryList();
		inventorylist.clearItems();
		for(int i=0; i<list.length; i++) {
			inventorylist.addItem(list[i].toString());
		}
	}
	
	public void updateStats(Player player) {
		StringBuilder statsString = new StringBuilder();
		statsString.append("Health: ");
		statsString.append(player.getHealth());
		statsString.append('\n');
		statsString.append("Damage: ");
		statsString.append(player.getEquipped().getDamage());
		statsString.append('\n');
		statsString.append("Level: ");
		statsString.append(player.getLevel());
		stats.setText(statsString.toString());
	}
	
	private void startGUI() {
		try {
			DefaultTerminalFactory terminalFactory = new DefaultTerminalFactory();
			screen = terminalFactory.createScreen();
			screen.startScreen();
			gui = new MultiWindowTextGUI(screen);
			
			//Create the window that will hold the panel
			Window worldview = new BasicWindow("Dagger Dungeon");
			LinkedList<Hint> worldviewhints = new LinkedList<Hint>();
			worldviewhints.add(Window.Hint.CENTERED);
			worldviewhints.add(Window.Hint.MODAL);
			worldviewhints.add(Window.Hint.FULL_SCREEN);
			worldview.setHints(worldviewhints);
			
			//Create the panel that will hold all of the components
			Panel panel = new Panel(new GridLayout(4));
			
			//Create the map which will take up most of the panel
			map.setLayoutData(GridLayout.createLayoutData(GridLayout.Alignment.BEGINNING, GridLayout.Alignment.BEGINNING, true, true, 3, 4));
			map.setPreferredSize(new TerminalSize(Level.MAPCOLUMNS,Level.MAPROWS));
			panel.addComponent(map);
			
			//Create the Inventory label and add the inventory combobox
			panel.addComponent(new Label("---Inventory---"));
			inventorylist.setPreferredSize(new TerminalSize(15, 5));
			inventorylist.setReadOnly(true);
			inventorylist.setDropDownNumberOfRows(5);
			panel.addComponent(inventorylist);
			
			//Create the Stats label and add the stats textBox
			panel.addComponent(new Label("---Stats---"));
			stats.setPreferredSize(new TerminalSize(15,3));
			panel.addComponent(stats);
			
			//Add everything and update the screen
			worldview.setComponent(panel);
			gui.addWindow(worldview);
			gui.updateScreen();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		} 
	}

	public KeyStroke getUserInput() throws IOException {
		return screen.pollInput();
	}
	
	public void updateMap(Level level) {
		map.setText(level.mapToString());
	}

	public void update() throws IOException {
		gui.updateScreen();
	}

	//This changes the focus to the inventory and lets the comboBox handle the user interaction until the user hits 'i'
	public void accessInventory(Player player) throws IOException {
		inventorylist.takeFocus();
		gui.updateScreen();
		while(true) {
			KeyStroke key = screen.readInput();
			inventorylist.handleInput(key);
			gui.updateScreen();
			if(key!=null)
				if(key.getCharacter() != null)
					if(key.getCharacter().charValue() == 'i')
						break;	
		}
		String item = inventorylist.getSelectedItem();
		player.equipItem(item);
		map.takeFocus();
	}

	public void promptToExit() throws IOException {
		MessageDialogBuilder exitmessage = new MessageDialogBuilder();
		exitmessage.setTitle("Exit?");
		exitmessage.setText("Are you sure you want to exit the game?");
		exitmessage.addButton(MessageDialogButton.Yes);
		exitmessage.addButton(MessageDialogButton.No);
		if(exitmessage.build().showDialog(gui) == MessageDialogButton.Yes) {
			System.out.println("Exiting game.");
			screen.close();
			System.exit(0);
		}
		
	}

	public void promptToRestart() throws IOException {
		MessageDialogBuilder restartmessage = new MessageDialogBuilder();
		restartmessage.setTitle("Restart?");
		restartmessage.setText("You have died. Would you like to start a new game?");
		restartmessage.addButton(MessageDialogButton.Yes);
		restartmessage.addButton(MessageDialogButton.No);
		if(restartmessage.build().showDialog(gui) == MessageDialogButton.No) {
			System.out.println("Exiting game.");
			screen.close();
			System.exit(0);
		}
		
	}

}
