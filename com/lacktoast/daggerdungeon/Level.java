package com.lacktoast.daggerdungeon;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;
import com.lacktoast.daggerdungeon.Characters.Characters;
import com.lacktoast.daggerdungeon.Characters.Demon;
import com.lacktoast.daggerdungeon.Characters.Devil;
import com.lacktoast.daggerdungeon.Characters.Goblin;
import com.lacktoast.daggerdungeon.Characters.Player;

public class Level {
	
	private char[][] mapstring;
	private LinkedList<Characters> enemylist;
	
	public static final int MAPROWS = 22;
	public static final int MAPCOLUMNS = 60;
	public static final int PLAYERSTARTROW = 2;
	public static final int PLAYERSTARTCOLUMN = 2;
	public static final int EXITROW = 19;
	public static final int EXITCOLUMN = 57;
	
	private static final char WALL = '█';
	private static final char EXIT = '⇲';
	private static final char DEADCHARACTER = '†';
	public static final char PLAYERICON = 'ጰ';
	
	
	public Level() {
		mapstring = new char[MAPROWS][MAPCOLUMNS];
		enemylist = new LinkedList<Characters>();
		this.generateLevel();
	}
	
	private void generateLevel() {
		padMapString();
		drawMapConstants();
		generateRooms();
		addEnemies();
	}

	private void addEnemies() { 
		//Generate a random number of enemies/enemy types
		for(int i=0; i<ThreadLocalRandom.current().nextInt(1, 6); i++) {
			int characterselect = ThreadLocalRandom.current().nextInt(0, 101);
			Characters enemy;
			if(characterselect<50) {
				enemy = new Goblin();
			} else if(characterselect>49 && characterselect<70) {
				enemy = new Demon();
			} else if(characterselect>=93) {
				enemy = new Devil();
			} else 
				enemy = new Goblin();
			enemylist.add(enemy);
		}
	
		//randomly place all of the enemies we just created throughout the map
		for(Characters c: enemylist) {
			boolean placed = false;
			while(!placed) {
				int row = ThreadLocalRandom.current().nextInt(4, MAPROWS-3);
				int column = ThreadLocalRandom.current().nextInt(4, MAPCOLUMNS-3);
				if(mapstring[row][column] == ' ') {
					c.setColumnPosition(column);
					c.setRowPosition(row);
					mapstring[row][column] = c.getIcon();
					placed = true;
				}
			}
		}
	}
	//This method will generate rooms by randomly creating vertical and horizontal walls throughout the map
	//two walls cannot be generated next to each other, i.e. walls can only be one character thick.
	//this method will then call the addDoors method to ensure the player can still get to the exit
	private void generateRooms() {
		//pick where vertical walls are located
		int vwallcount = ThreadLocalRandom.current().nextInt(1, 9);
		HashSet<Integer> vwallset = new HashSet<Integer>();
		for(int i=0; i<vwallcount; i++) {
			Integer location = new Integer(ThreadLocalRandom.current().nextInt(3, MAPCOLUMNS-3));
			if(!vwallset.contains(location-1) && !vwallset.contains(location+1))
				vwallset.add(location);
		}
		
		//pick where horizontal walls are located
		int hwallcount = ThreadLocalRandom.current().nextInt(1, 6);
		HashSet<Integer> hwallset = new HashSet<Integer>();
		for(int i=0; i<hwallcount; i++) {
			Integer location = new Integer(ThreadLocalRandom.current().nextInt(3, MAPROWS-3));
			if(!hwallset.contains(location-1) && !hwallset.contains(location+1))
				hwallset.add(location);
		}
		
		//draw vertical walls
		for(Integer w: vwallset) {
			int wallplacement = w.intValue();
			for(int j=2; j<MAPROWS-2; j++)
					mapstring[j][wallplacement] = WALL;
		}
		
		//draw horizontal walls
		for(Integer w: hwallset) {
			int wallplacement = w.intValue();
			for(int j=2; j<MAPCOLUMNS-2; j++)
				mapstring[wallplacement][j] = WALL;	
		}
		addDoors(vwallset,hwallset);
	}
	
	//This method will go through all of the generated walls. For each wall that was generated a door will be added between every wall it intersects with
	//There is also a chance that part of the wall will be removed completely, this makes the levels less uniform
	private void addDoors(HashSet<Integer> vwallset, HashSet<Integer> hwallset) {
		boolean removewall = false;
		
		for(Integer w: vwallset) {
			int wallplacement = w.intValue();
			int lastwall = 0;
			for(int j=2; j<MAPROWS-2; j++) {
				if(removewall && (mapstring[j][wallplacement+1] != WALL && mapstring[j][wallplacement-1] != WALL))//We are removing the wall and we haven't hit another intersecting wall
					mapstring[j][wallplacement] = ' ';
				if((mapstring[j][wallplacement+1] == WALL || mapstring[j][wallplacement-1] == WALL) || j == MAPROWS-3) { //We have found an intersecting wall or reached the outer wall
					if(!removewall)
						mapstring[j-((j-lastwall)/2)][wallplacement] = ' ';
					if(ThreadLocalRandom.current().nextInt(0, 10)>6) {
						removewall = true;
					} else
						removewall = false;
					lastwall = j;
				}
			}
		}
		
		for(Integer w: hwallset) {
			int wallplacement = w.intValue();
			int lastwall = 0;
			for(int j=2; j<MAPCOLUMNS-2; j++) {
				if(removewall && (mapstring[wallplacement+1][j] != WALL && mapstring[wallplacement-1][j] != WALL))//We are removing the wall and we haven't hit another intersecting wall
					mapstring[wallplacement][j] = ' ';	
				if((mapstring[wallplacement+1][j] == WALL || mapstring[wallplacement-1][j] == WALL) || j==MAPCOLUMNS-3) {//We have found an intersecting wall or reached the outer wall
					if(!removewall)
						mapstring[wallplacement][j-((j-lastwall)/2)] = ' ';
					if(ThreadLocalRandom.current().nextInt(0, 10)>6) {
						removewall = true;
					} else
						removewall = false;
					lastwall = j;				
				}
			}
		}
	}

	//This draws the constants on each level. This includes the outside walls, the exit icon, and the player's start position
	private void drawMapConstants() {
		for(int i=1; i<MAPROWS-2; i++) {
			mapstring[i][1] = WALL;
			mapstring[i][MAPCOLUMNS-2] = WALL;
		}
		for(int i=1; i<MAPCOLUMNS-1; i++) {
			mapstring[1][i] = WALL;
			mapstring[MAPROWS-2][i] = WALL;
		}
		mapstring[PLAYERSTARTROW][PLAYERSTARTCOLUMN] = PLAYERICON;		
		mapstring[EXITROW][EXITCOLUMN] = EXIT;
	}
	
	//This fills the char array that we will be using to draw the map. Newline characters are added because the TextBox does not wrap the text
	private void padMapString() {
		for(int i=0; i<MAPROWS; i++) {
			Arrays.fill(mapstring[i], ' ');
			mapstring[i][MAPCOLUMNS-1] = '\n';
		}
	}
	
	//This converts the char array to a string that will be displayed in the TextBox
	public String mapToString() {
		StringBuilder sb = new StringBuilder(MAPROWS*MAPCOLUMNS);
		for(int i=0; i<MAPROWS;i++) {
			for(int j=0; j<MAPCOLUMNS;j++)
				sb.append(mapstring[i][j]);
		}
		return sb.toString();
	}
	
	//This will move the character e toward the player this is only called by enemyActions
	private void moveTowardPlayer(Characters e, Player player) {
		int rowoffset = Math.abs(e.getRowPosition()-player.getRowPosition());
		int columnoffset = Math.abs(e.getColumnPosition()-player.getColumnPosition());
		
		if(rowoffset>columnoffset) {
			if(e.getRowPosition()-player.getRowPosition()>0) {
				validMoveUp(e);
			} else
				validMoveDown(e);
		} else {
			if(e.getColumnPosition()-player.getColumnPosition()>0) {
				validMoveLeft(e);
			} else {
				validMoveRight(e);
			}
		}
	}

	//This handles the basic ai. For every enemy on the map if the player is within 6 units of the character the character will try to move toward the player.
	//If the character is right next to the player the character will attack.
	public void enemyActions(Player player) {
		for(int i=0; i<enemylist.size(); i++) {
			Characters e = enemylist.get(i);
			if(e.getHealth()<1) {
				mapstring[e.getRowPosition()][e.getColumnPosition()] = DEADCHARACTER;
				enemylist.remove(i);
				player.weaponPickup(e.getEquipped());
				player.takeHealthPotion(e.dropHealthPotion());
			}else {
				int rowoffset = Math.abs(e.getRowPosition()-player.getRowPosition());
				int columnoffset = Math.abs(e.getColumnPosition()-player.getColumnPosition());
				if((rowoffset == 1 && columnoffset == 0) || (columnoffset == 1  && rowoffset == 0)) {
					player.takeDamage(e.getEquipped().getDamage());
				} else if (rowoffset<6 && columnoffset<6)
					moveTowardPlayer(e, player);
			}
		}	
	}
	
	//When the player attacks this method will check if any enemies are within range and if they are it applies the damage.
	public void playerAttack(Player player) {
		int playerrow = player.getRowPosition();
		int playercolumn = player.getColumnPosition();
		
		for(Characters e : enemylist) {
			int erow = e.getRowPosition();
			int ecolumn = e.getColumnPosition();
			if((erow-1==playerrow || erow+1==playerrow) && playercolumn==ecolumn) {
				e.takeDamage(player.getEquipped().getDamage());
			} else if((ecolumn-1==playercolumn || ecolumn+1==playercolumn) && playerrow==erow)
				e.takeDamage(player.getEquipped().getDamage());
		}
		
	}
	
	//The valid move methods will verify that a character is able to move the direction they are trying to move.
	//If the move is valid, the corresponding updateCharacterPosition method is called.
	public boolean validMoveUp(Characters character) {
		char location = mapstring[character.getRowPosition()-1][character.getColumnPosition()];
		boolean valid = location == ' ' || location == EXIT || location == DEADCHARACTER;
		if(valid)
			updateCharacterPositionUp(character);
		return valid;
	}
	
	public boolean validMoveDown(Characters character) {
		char location = mapstring[character.getRowPosition()+1][character.getColumnPosition()];
		boolean valid = location == ' ' || location == EXIT || location == DEADCHARACTER;
		if(valid)
			updateCharacterPositionDown(character);
		return valid;
	}

	public boolean validMoveRight(Characters character) {
		char location = mapstring[character.getRowPosition()][character.getColumnPosition()+1];
		boolean valid = location == ' ' || location == EXIT || location == DEADCHARACTER;
		if(valid)
			updateCharacterPositionRight(character);
		return valid;
	}
	
	public boolean validMoveLeft(Characters character) {
		char location = mapstring[character.getRowPosition()][character.getColumnPosition()-1];
		boolean valid = location == ' ' || location == EXIT || location == DEADCHARACTER;
		if(valid)
			updateCharacterPositionLeft(character);
		return valid;
	}
	
	//The update character position methods will move the character and make sure the mapstring is updated properly.
	//These methods do not verify the move is valid so they are only called from the validMove methods.
	private void updateCharacterPositionUp(Characters character) {
		mapstring[character.getRowPosition()][character.getColumnPosition()] = ' ';
		character.moveUp();
		mapstring[character.getRowPosition()][character.getColumnPosition()] = character.getIcon();
	}
	private void updateCharacterPositionDown(Characters character) {
		mapstring[character.getRowPosition()][character.getColumnPosition()] = ' ';
		character.moveDown();
		mapstring[character.getRowPosition()][character.getColumnPosition()] = character.getIcon();
	}
	private void updateCharacterPositionLeft(Characters character) {
		mapstring[character.getRowPosition()][character.getColumnPosition()] = ' ';
		character.moveLeft();
		mapstring[character.getRowPosition()][character.getColumnPosition()] = character.getIcon();
	}
	private void updateCharacterPositionRight(Characters character) {
		mapstring[character.getRowPosition()][character.getColumnPosition()] = ' ';
		character.moveRight();
		mapstring[character.getRowPosition()][character.getColumnPosition()] = character.getIcon();
	}
}
