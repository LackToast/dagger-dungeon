package com.lacktoast.daggerdungeon;

public class Main {

	public static void main(String[] args) {
		System.out.println("Welcome to DaggerDungeon");
		System.out.println("Starting a new game and initilizing the GUI...");
		Game newgame = new Game();
		System.out.println("Starting gameplay loop.");
		while(true) {
			newgame.gameLoop();//If this ever returns we will just start a new game.
			System.out.println("Starting new game.");
			newgame = new Game();
		}
	}

}
