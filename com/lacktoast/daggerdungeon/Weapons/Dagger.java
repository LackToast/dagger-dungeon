package com.lacktoast.daggerdungeon.Weapons;

//The dagger is the most basic weapon and what the player always spawns with. It does 1 damage.
public class Dagger extends Weapon{
	
	public Dagger() {
		super(1);
	}

	public String toString() {
		return "Dagger";
	}
}
