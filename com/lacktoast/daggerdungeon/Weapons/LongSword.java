package com.lacktoast.daggerdungeon.Weapons;

//The LongSword is the most powerful weapon, it does 10 damage.
public class LongSword extends Weapon{
	
	public LongSword() {
		super(10);
	}

	public String toString() {
		return "Long Sword";
	}
}
