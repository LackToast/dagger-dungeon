package com.lacktoast.daggerdungeon.Weapons;

//The Rapier is slightly better than the dagger, it does 2 damage
public class Rapier extends Weapon{
	
	public Rapier() {
		super(2);
	}

	public String toString() {
		return "Rapier";
	}
}
