package com.lacktoast.daggerdungeon.Weapons;

//The ShortSword is a decent weapon that does 5 damage.
public class ShortSword extends Weapon{
	
	public ShortSword() {
		super(5);
	}

	public String toString() {
		return "Short Sword";
	}
}
