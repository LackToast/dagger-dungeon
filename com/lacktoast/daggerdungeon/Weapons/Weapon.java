package com.lacktoast.daggerdungeon.Weapons;

public abstract class Weapon {

	private int damage;
	
	public Weapon(int damage) {
		this.damage = damage;
	}
	
	public int getDamage() {
		return damage;
	}
	
	public boolean equals(Object o) {;
		return this.getClass() == o.getClass();
	}
	
	public int hashCode() {
		return this.getDamage();
	}

}